# EXAMPLE - HTTP REQUEST TO API NAICS IDENTIFIER

## Getting Started

Following these instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Python interpreter needs to run the software:

|   Sofware    |   Version   |
| :---------:  | :---------: |
|   Python     |  3.6        |
|   Requests   |  2.26.0     |


pip3 install -r requirements.txt should be executed for to install **request 2.26.0** library

## Usage

In order to run the process, make sure that you have the proper token to send requests. Once you have the token, follow the next steps to execute the code:

1. Replace '**YOUR_TOKEN_HERE**' with your **token**
2. Run the code

**ATTENTION**: It is necessary to run the code with an IDE to view the results in console.

## Built With

* [PyCharm](https://www.jetbrains.com/idea/) - The IDE for Python used
* [Python](https://www.python.org/) - Programming language
* [Github](https://github.com/) - Code Hosting Platform for Version Control

## Authors

* **Ramiro Bastar González** - *Data Scientist* - ramiro@relativity6.com

## License

This project is private, owned by "Relativity6"
