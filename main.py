import json
import requests


def prediction():
    url='https://api.usemarvin.ai/naics/naics/search/'
    values = {
        "token": "YOUR_TOKEN_HERE",
        "name": "Apple Inc.",
        "address": "One Apple Park Way",
        "state": "CA"
    }
    headers = {'content-type': 'application/json'}

    response = requests.post(url, data=json.dumps(values), headers=headers)
    return response.json()

if __name__ == '__main__':
    print(prediction())

